var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var fs = require('fs');
var youtubedl = require('youtube-dl');
var downloading = false;

var users = [];
var bannedips = [];


app.get('/', function(req, res) {
    res.sendFile(__dirname + '/index.html');
});

app.get('/main.css', function(req, res) {
    res.sendFile(__dirname + '/main.css');
});

app.get('/music', function(req, res) {
    res.sendFile(__dirname + '/myvideo.mp3');
});

http.listen(3000, function() {
    console.log('listening on *:3000');
});

io.on('connection', function(socket) {

    console.log('a user connected!');
    users.push({socket:socket.id, time:-1, volume:-1, ping:-1, pingData:[]});

    socket.on('makeadmin', function(data) {

    });

    socket.on('banuser', function(data) {
        
    });

    socket.on('sync', function(data) {
        if(socket.id != users[0].socket) return; 
        io.to(data.socket).emit('msg',{sync:data.type});
    });

    socket.on('msg', function(data) {
        if(socket.id != users[0].socket) return; 

        if (data.change != undefined && downloading == false) {
            downloading = true;
            video = youtubedl(data.change, ['-x', '--extract-audio', '--audio-format', 'mp3', '--audio-quality', '7'], {
                cwd: __dirname
            });

            video.on('info', function(info) {
                console.log('Download started');
                console.log('filename: ' + info._filename);
                console.log('size: ' + info.size);
            });

            video.on('end', function() {
                console.log('finished downloading!');
                downloading = false;
                io.emit('msg', {
                    'change': true
                });
            });

            video.pipe(fs.createWriteStream('myvideo.mp3', {
                flags: 'r+'
            }));
        } else{
            io.emit('msg', data);
        }
    });

    socket.on('stats', function(data) {
        var index = getUserIdBySocket(socket.id);

        users[index].name = data.name.substring(0,16);
        users[index].time = data.time;
        users[index].volume = data.volume;        

        users[index].pingData.push(getTime() - data.requestTime);
        if(users[index].pingData.length > 20){
            users[index].pingData.shift();
        }

        users[index].ping = getAveragePing(users[index].pingData);

    });

    socket.on('disconnect', function() {
        //var index = users.map(x => {return x.socket;  }).indexOf(socket.id);
        users.splice(getUserIdBySocket(socket.id), 1);
        console.log('user disconnected');
    });
});

function getUserIdBySocket(socket){
    return users.map(x => {return x.socket;  }).indexOf(socket);
}

function getTime(){
    return new Date().getTime();
}

function getAveragePing(pingArray){
    var total = 0;
    for (var i = pingArray.length - 1; i >= 0; i--) {
        total += pingArray[i];
    }
    return Math.round(total/pingArray.length);
}

setInterval(interval , 100);
function interval(){
    //console.log(users);
    io.emit('msg', {userlist:users,requestTime: getTime()});
}